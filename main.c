/* 
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "stdio.h"
#include "stdlib.h"
#include "time.h"

int attempts = 1;
int successes[] = {0, 0, 0};
int firstSuccess[] = {-1, -1, -1};
int odds;

int main(){
	srand(time(NULL));
	printf("This program will roll the odds a million times, and tell you when the first successful roll was, and the total amount of successful rolls.\nThis program is licensed under the GPL-3.0.\n\n");
	printf("Enter odds: 1 in ");
	scanf("%i", &odds);
	int result = 0;
	int i = 0;
	reroll:
	attempts = 0;
	while (attempts != 1000001){
		result = rand() % odds;
		if (result == 0){
			while (result == 0){
				result = rand() % odds;
			}
		}
		if (result == 1){
			successes[i]++;
			if (firstSuccess[i] == -1){
				firstSuccess[i] = attempts;
			}
		}
		attempts++;
	}
	printf("\nFirst Success: %i\nTotal Number of Successes: %i\n", firstSuccess[i], successes[i]);
	i++;
	if ( firstSuccess[i] == -1 ) {
		goto reroll;
	}
	int average;
	average = firstSuccess[0] + firstSuccess[1] + firstSuccess[2] / 3;
	printf("Average: %i\n", average);
	exit(0);
	return 0;
}
