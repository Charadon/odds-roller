<b>How to Compile:</b>
<br />
Run this command: <i>gcc main.c -o rng</i>

<b>What this program does:</b>
<br />
This program will roll the odds a million times, and tell you when the first successful roll was, and the total amount of successful rolls. This is useful for when you want to calculate how many times you'll have to do an odd before you are finally successful.